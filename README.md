# Configure local environments

1. Go to your favorite development folder. Eg `cd ~/development`
1. Create base directory `mkdir americanoutlets && cd americanoutlets`
1. Clone this repository `git clone git@github.com:HeadMage/americanoutlets_docker.git .`
1. Create portal directory and clone `mkdir -p htdocs/portal && git clone git@bitbucket.org:levtechdev/portal-laravel.git htdocs/portal` 
1. Create magento directory and clone `mkdir -p htdocs/magento && git clone git@bitbucket.org:levtechdev/americanoutlets-magento.git htdocs/magento` 
1. Add domains to hosts file 
    ```
    127.0.0.1 americanoutlets.loc
    127.0.0.1 portal.americanoutlets.loc
    ```
1. Create data directories 
    ```
    mkdir -p docker/data/portal-mysql \
    && mkdir -p docker/data/portal-mysql \
    && mkdir -p docker/data/portal-redis \
    && mkdir -p docker/data/magento-maria \
    && mkdir -p docker/data/magento-redis \
    && mkdir -p docker/data/magento-redis-fpc \
    && mkdir -p docker/data/magento-elastic
    ```
1. Download dumps
    - Portal MySQL
        ```
        curl -L -o docker/install/portal-mysql/dump.sql.gz https://s3-eu-west-1.amazonaws.com/americanoutlets.com/db_dump_portal.americanoutlets.com_11.26.18.sql.gz \
        && gunzip -c -k docker/data/portal-mysql/dump.sql.gz > docker/install/portal-mysql/dump.sql
        ```
    - Portal Mongo
        ```
        curl -L -o docker/install/portal-mongo/attributes_collection.bson https://s3-eu-west-1.amazonaws.com/americanoutlets.com/attributes_collection.bson \
        && curl -L -o docker/install/portal-mongo/csv_mapping_collection.bson https://s3-eu-west-1.amazonaws.com/americanoutlets.com/csv_mapping_collection.bson \
        && curl -L -o docker/install/portal-mongo/products_collection.bson https://s3-eu-west-1.amazonaws.com/americanoutlets.com/products_collection.bson
        ```
     - Magento MySQL
        ```
        curl -L -o docker/install/magento-maria/dump.sql.gz https://www.dropbox.com/s/5k7wvvztdpib48q/db_dump_stage.americanoutlets.com_10.25.18.sql.gz?dl=1 \
        && gunzip -c -k docker/install/magento-maria/dump.sql.gz > docker/install/magento-maria/dump.sql
        ```
1. Install Docker ad Docker compose
    - https://www.docker.com/get-started
    - https://docs.docker.com/compose/install/#install-compose
1. Run docker compose `docker-compose up`
1. Import dumps
    - Portal MySQL
        - Login to container `docker exec -it $(docker ps -q -f "name=americanoutlets_portal-mysql") /bin/bash`
        - Run import `mysql -u dev -pdev -f db < /home/install/dump.sql`
        - Logout `exit`
    - Portal Mongo
        - Login to container `docker exec -it $(docker ps -q -f "name=americanoutlets_portal-mongo") /bin/bash`
        - Run import
            ```
            mongorestore -d local -c attributes_collection /home/install/attributes_collection.bson \
            && mongorestore -d local -c csv_mapping_collection /home/install/csv_mapping_collection.bson \
            && mongorestore -d local -c products_collection /home/install/products_collection.bson \
            ```
        - Logout `exit`
    - Magento MySQL
        - Login to container `docker exec -it $(docker ps -q -f "name=americanoutlets_magento-mariadb") /bin/bash`
        - Run import `mysql -u root -pdev -f db < /home/install/dump.sql`
        - Login to mysql `mysql -u root -pdev`
        - Switch DB `use db;`
        - Run script
            ```
            UPDATE admin_user SET password = CONCAT(SHA2('xxxxxxx123123q', 256), ':xxxxxxx:1'), username = 'admin25' WHERE username = 'maks';
            UPDATE core_config_data SET value = 'http://americanoutlets.loc/' WHERE path = 'web/unsecure/base_url';
            UPDATE core_config_data SET value = 'http://americanoutlets.loc' WHERE path = 'web/secure/base_url';
            UPDATE core_config_data SET value = '' WHERE path = 'web/cookie/cookie_domain';
            DELETE FROM core_config_data WHERE path LIKE "catalog/search%";
            UPDATE core_config_data SET value = 'magento-elasticsearch:9200' WHERE path = 'smile_elasticsuite_core_base_settings/es_client/servers';
            ```
        - Logout `exit`
1. Configure portal
    - Go to portal directory `cd htdocs/portal`
    - Switch to `development` branch `git checkout -t origin/development`
    - Setup .env config file `cp ../config/portal/.env .`
    - Login to container `docker exec -it $(docker ps -q -f "name=americanoutlets_portal-web") /bin/bash`
    - Run commands
        - `composer install --no-dev`
        - `php artisan key:generate`
        - `ln -s /usr/bin/nodejs /usr/bin/node`
        - `npm install`
        - `npm i --save-dev popper.js`
        - `npm install`
        - `npm run prod`
    - Logout `exit`
1. Configure magento
    - Go to portal directory `cd ../magento`
    - Setup env config file `cp ../config/magento/env.php app/etc/env.php`
    - Login to container `docker exec -it $(docker ps -q -f "name=americanoutlets_magento-web") /bin/bash`
    - Configure `auth.json` https://confluence.atlassian.com/bitbucket/oauth-on-bitbucket-cloud-238027431.html
        ```
        {
            "http-basic": {
                "repo.magento.com": {
                    "username": "dd8daba5d950d88c5b94e2e1bdf3934d",
                    "password": "d8b63abacec83b3d68633f6ccac92f78"
                }
            },
            "bitbucket-oauth": {
                "bitbucket.org": {
                    "consumer-key": "XXX",
                    "consumer-secret": "YYY"
                }
            }
        }
        ```
    - Run commands
        - `composer install`
        - If you see error `chdir(): No such file or directory (errno 2)` then restart docker and run `composer install` again
        - If you see error `The checksum verification of the file failed (downloaded from https://repo.magento.com/archives/vertex/sdk/vertex-sdk-1.0.0.0.zip)` then run `composer update klarna/module-ordermanagement` and then `composer install`
        - `bin/magento cache:flush`
        - `rm -Rf var/cache/* var/page_cache/* var/view_preprocessed var/di/* generated/* pub/static/*`
        - `bin/magento setup:upgrade`
        - `bin/magento setup:di:compile` ????
        - `bin/magento deploy:mode:set --skip-compilation developer`
        - `bin/magento setup:upgrade`
        - `composer dump-autoload --optimize`
        - `bin/magento setup:static-content:deploy he_IL en_US --theme=Magento/backend --theme=Fisha/americanoutlets --jobs 1 -f`
        - `bin/magento setup:upgrade`
    - Logout `exit`

2. Configure connection between projects

2.1 portal project
    - change variable in .env
     MAGENTO_API_BASE_URL=http://magento-web/

3. Run projects

only magento run command
    - ./start_app.sh
only portal
    - ./start_app.sh portal
both apps
    - ./start_app.sh ao portal    