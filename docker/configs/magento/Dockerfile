FROM ubuntu:xenial
MAINTAINER atlogex <atlogex@gmail.com>

ENV OS_LOCALE="en_US.UTF-8"
RUN apt-get update && apt-get install -y locales && locale-gen ${OS_LOCALE}
ENV LANG=${OS_LOCALE} \
    LANGUAGE=${OS_LOCALE} \
    LC_ALL=${OS_LOCALE} \
    DEBIAN_FRONTEND=noninteractive

ENV APACHE_CONF_DIR=/etc/apache2 \
    PHP_CONF_DIR=/etc/php/7.1 \
    PHP_DATA_DIR=/var/lib/php

COPY entrypoint.sh /sbin/entrypoint.sh

RUN \
    BUILD_DEPS='software-properties-common python-software-properties' \
    && apt-get update && apt-get install -y \
    && dpkg-reconfigure locales \
    && apt-get install --no-install-recommends -y $BUILD_DEPS \
    && add-apt-repository -y ppa:ondrej/php \
    && add-apt-repository -y ppa:ondrej/apache2 \
    && apt-get update \
    && apt-get -y --no-install-recommends install curl apache2 libapache2-mod-php7.1 php-pear\
    php7.1-cli \
    php7.1-curl \
    php7.1-gd \
    php7.1-iconv \
    php7.1-mbstring \
    php7.1-mcrypt \
    php7.1-memcached \
    php7.1-mongodb \
    php7.1-oauth \
    php7.1-mysql \
    php7.1-redis \
    php7.1-xml \
    php7.1-soap \
    php7.1-sqlite3 \
    php7.1-ssh2 \
    php7.1-zip \
    php7.1-json \
    php7.1-xsl \
    php7.1-intl \
    php7.1-opcache \
    php7.1-readline \
    php7.1-xdebug \
    php7.1-imagick \
    php7.1-readline \
    php7.1-mbstring \
    php7.1-xdebug \
    php7.1-bcmath \
    cron nano rsyslog mc \
    python2.7 python-pip \
    nodejs npm \
    # Apache settings
    && cp /dev/null ${APACHE_CONF_DIR}/conf-available/other-vhosts-access-log.conf \
    && rm ${APACHE_CONF_DIR}/sites-enabled/000-default.conf ${APACHE_CONF_DIR}/sites-available/000-default.conf \
    && a2enmod rewrite php7.1 \
    && a2enmod ssl php7.1 \
    # PHP settings
    && phpenmod mcrypt \
    # Install composer
    && curl -sS https://getcomposer.org/installer | php -- --version=1.6.4 --install-dir=/usr/local/bin --filename=composer \
    # Cleaning
    && apt-get purge -y --auto-remove $BUILD_DEPS \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    # Forward request and error logs to docker log collector
    && ln -sf /dev/stdout /var/log/apache2/access.log \
    && ln -sf /dev/stderr /var/log/apache2/error.log \
    && chmod 755 /sbin/entrypoint.sh \
    && chown www-data:www-data ${PHP_DATA_DIR} -Rf

COPY ./apache2.conf ${APACHE_CONF_DIR}/apache2.conf
COPY ./app.conf ${APACHE_CONF_DIR}/sites-enabled/app.conf
COPY ./php.ini  ${PHP_CONF_DIR}/apache2/conf.d/custom.ini

RUN apt-get update && apt-get install -y git 

# ioncube loader here
RUN curl -o ioncube.tar.gz http://downloads3.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz \
    && tar -xvvzf ioncube.tar.gz \
    && mkdir /usr/lib/php/modules/ \
    && mv ioncube/ioncube_loader_lin_7.1.so /usr/lib/php/modules/ \
    && rm -Rf ioncube.tar.gz ioncube \
    && echo 'zend_extension = /usr/lib/php/modules/ioncube_loader_lin_7.1.so' > /etc/php/7.1/apache2/conf.d/00-ioncube.ini \
    && chown www-data:www-data ${PHP_DATA_DIR} -Rf

WORKDIR /var/www

EXPOSE 80 443

# By default, simply start apache.
CMD ["/sbin/entrypoint.sh"]
