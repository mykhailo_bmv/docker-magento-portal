#!/usr/bin/env bash

mongorestore -d local -c attributes_collection /home/install/attributes_collection.bson
mongorestore -d local -c csv_mapping_collection /home/install/csv_mapping_collection.bson
mongorestore -d local -c products_collection /home/install/products_collection.bson
