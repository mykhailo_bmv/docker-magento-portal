<?php
return array (
  'cache_types' => 
  array (
    'compiled_config' => 0,
    'config' => 1,
    'layout' => 1,
    'block_html' => 1,
    'collections' => 1,
    'reflection' => 1,
    'db_ddl' => 1,
    'eav' => 1,
    'customer_notification' => 1,
    'target_rule' => 1,
    'full_page' => 0,
    'config_integration' => 1,
    'config_integration_api' => 1,
    'translate' => 1,
    'config_webservice' => 1,
    'config_urapidflow' => 1,
  ),
  'backend' => 
  array (
    'frontName' => 'admin',
  ),
  'db' => 
  array (
    'connection' => 
    array (
      'indexer' => 
      array (
        'host' => 'magento-mariadb',
        'dbname' => 'db',
        'username' => 'root',
        'password' => 'dev',
        'model' => 'mysql4',
        'engine' => 'innodb',
        'initStatements' => 'SET NAMES utf8;',
        'active' => '1',
        'persistent' => NULL,
      ),
      'default' => 
      array (
        'host' => 'magento-mariadb',
        'dbname' => 'db',
        'username' => 'root',
        'password' => 'dev',
        'model' => 'mysql4',
        'engine' => 'innodb',
        'initStatements' => 'SET NAMES utf8;',
        'active' => '1',
      ),
    ),
    'table_prefix' => '',
  ),
  'crypt' => 
  array (
    'key' => '3b737bc764cec4ba85f02e526ea0378d',
  ),
  'session' => 
  array (
    'save' => 'redis',
    'redis' => 
    array (
      'host' => 'magento-redis',
      'port' => '6379',
      'password' => '',
      'timeout' => '2.5',
      'persistent_identifier' => '',
      'database' => '2',
      'compression_threshold' => '2048',
      'compression_library' => 'gzip',
      'log_level' => '1',
      'max_concurrency' => '6',
      'break_after_frontend' => '5',
      'break_after_adminhtml' => '30',
      'first_lifetime' => '600',
      'bot_first_lifetime' => '60',
      'bot_lifetime' => '7200',
      'disable_locking' => '0',
      'min_lifetime' => '60',
      'max_lifetime' => '2592000',
    ),
  ),
  'resource' => 
  array (
    'default_setup' => 
    array (
      'connection' => 'default',
    ),
  ),
  'x-frame-options' => 'SAMEORIGIN',
  'MAGE_MODE' => 'default',
  'install' => 
  array (
    'date' => 'Wed, 06 Sep 2017 17:56:16 +0000',
  ),
  'cache' => 
  array (
    'frontend' => 
    array (
      'default' => 
      array (
        'backend' => 'Cm_Cache_Backend_Redis',
        'backend_options' => 
        array (
          'server' => 'magento-redis',
          'port' => 6379,
          'database' => 1,
        ),
      ),
      'page_cache' => 
      array (
        'backend' => 'Cm_Cache_Backend_Redis',
        'backend_options' => 
        array (
          'server' => 'magento-redis',
          'port' => 6379,
          'database' => 1,
        ),
      ),
    ),
  ),
  'system' => 
  array (
    'default' => 
    array (
      'dev' => 
      array (
        'js' => 
        array (
          'session_storage_key' => 'collected_errors',
        ),
        'restrict' => 
        array (
          'allow_ips' => NULL,
        ),
      ),
      'system' => 
      array (
        'smtp' => 
        array (
          'host' => 'localhost',
          'port' => '25',
        ),
        'magento_scheduled_import_export_log' => 
        array (
          'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
        ),
      ),
      'web' => 
      array (
        'unsecure' => 
        array (
          'base_url' => 'http://americanoutlets.loc/',
          'base_link_url' => '{{unsecure_base_url}}',
          'base_static_url' => NULL,
          'base_media_url' => NULL,
        ),
        'secure' => 
        array (
          'base_url' => 'http://americanoutlets.loc/',
          'base_link_url' => '{{secure_base_url}}',
          'base_static_url' => NULL,
          'base_media_url' => NULL,
        ),
        'default' => 
        array (
          'front' => 'cms',
        ),
        'cookie' => 
        array (
          'cookie_path' => NULL,
          'cookie_domain' => NULL,
        ),
      ),
      'admin' => 
      array (
        'url' => 
        array (
          'custom' => NULL,
        ),
      ),
      'currency' => 
      array (
        'import' => 
        array (
          'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
        ),
      ),
      'customer' => 
      array (
        'create_account' => 
        array (
          'email_domain' => 'example.com',
        ),
      ),
      'catalog' => 
      array (
        'search' => 
        array (
          'elasticsearch_server_hostname' => 'magento-elasticsearch',
          'elasticsearch_server_port' => '9200',
          'elasticsearch_index_prefix' => 'magento2',
          'elasticsearch_enable_auth' => '0',
          'elasticsearch_server_timeout' => '15',
          'elasticsearch5_server_hostname' => 'magento-elasticsearch',
          'elasticsearch5_server_port' => '9200',
          'elasticsearch5_index_prefix' => 'magento2',
          'elasticsearch5_enable_auth' => '0',
          'elasticsearch5_server_timeout' => '15',
          'solr_server_hostname' => 'localhost',
          'solr_server_port' => '8983',
          'solr_server_username' => 'admin',
          'solr_server_timeout' => '15',
          'solr_server_path' => 'solr',
        ),
        'productalert_cron' => 
        array (
          'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
        ),
        'product_video' => 
        array (
          'youtube_api_key' => NULL,
        ),
      ),
      'payment' => 
      array (
        'cybersource' => 
        array (
          'sandbox_flag' => '1',
          'access_key' => NULL,
          'profile_id' => NULL,
          'secret_key' => NULL,
          'merchant_id' => NULL,
          'transaction_key' => NULL,
        ),
        'authorizenet_directpost' => 
        array (
          'debug' => '0',
          'email_customer' => '0',
          'login' => NULL,
          'merchant_email' => NULL,
          'test' => '1',
          'trans_key' => NULL,
          'trans_md5' => NULL,
          'cgi_url' => 'https://secure.authorize.net/gateway/transact.dll',
          'cgi_url_td' => 'https://api2.authorize.net/xml/v1/request.api',
        ),
        'eway' => 
        array (
          'sandbox_flag' => '1',
          'live_api_key' => NULL,
          'live_api_password' => NULL,
          'live_encryption_key' => NULL,
          'sandbox_api_key' => NULL,
          'sandbox_api_password' => NULL,
          'sandbox_encryption_key' => NULL,
        ),
        'checkmo' => 
        array (
          'mailing_address' => NULL,
        ),
        'paypal_express' => 
        array (
          'debug' => '0',
        ),
        'paypal_express_bml' => 
        array (
          'publisher_id' => NULL,
        ),
        'payflow_express' => 
        array (
          'debug' => '0',
        ),
        'payflowpro' => 
        array (
          'user' => NULL,
          'pwd' => NULL,
          'partner' => NULL,
          'sandbox_flag' => '0',
          'debug' => '0',
        ),
        'paypal_billing_agreement' => 
        array (
          'debug' => '0',
        ),
        'payflow_link' => 
        array (
          'pwd' => NULL,
          'url_method' => 'GET',
          'sandbox_flag' => '0',
          'use_proxy' => '0',
          'debug' => '0',
        ),
        'payflow_advanced' => 
        array (
          'user' => 'PayPal',
          'pwd' => NULL,
          'url_method' => 'GET',
          'sandbox_flag' => '0',
          'debug' => '0',
        ),
        'braintree' => 
        array (
          'private_key' => NULL,
          'merchant_id' => NULL,
          'merchant_account_id' => NULL,
          'descriptor_phone' => NULL,
          'descriptor_url' => NULL,
        ),
        'braintree_paypal' => 
        array (
          'merchant_name_override' => NULL,
        ),
        'worldpay' => 
        array (
          'response_password' => NULL,
          'auth_password' => NULL,
          'md5_secret' => NULL,
          'sandbox_flag' => '1',
          'signature_fields' => 'instId:cartId:amount:currency',
          'installation_id' => NULL,
        ),
      ),
      'promo' => 
      array (
        'magento_reminder' => 
        array (
          'identity' => 'general',
        ),
      ),
      'contact' => 
      array (
        'email' => 
        array (
          'recipient_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
        ),
      ),
      'analytics' => 
      array (
        'url' => 
        array (
          'signup' => 'https://advancedreporting.rjmetrics.com/signup',
          'update' => 'https://advancedreporting.rjmetrics.com/update',
          'bi_essentials' => 'https://dashboard.rjmetrics.com/v2/magento/signup',
          'otp' => 'https://advancedreporting.rjmetrics.com/otp',
          'report' => 'https://advancedreporting.rjmetrics.com/report',
          'notify_data_changed' => 'https://advancedreporting.rjmetrics.com/report',
        ),
      ),
      'carriers' => 
      array (
        'dhl' => 
        array (
          'account' => NULL,
          'gateway_url' => 'https://xmlpi-ea.dhl.com/XMLShippingServlet',
          'id' => NULL,
          'password' => NULL,
          'debug' => '0',
        ),
        'fedex' => 
        array (
          'account' => NULL,
          'meter_number' => NULL,
          'key' => NULL,
          'password' => NULL,
          'sandbox_mode' => '0',
          'production_webservices_url' => 'https://ws.fedex.com:443/web-services/',
          'sandbox_webservices_url' => 'https://wsbeta.fedex.com:443/web-services/',
          'smartpost_hubid' => NULL,
        ),
        'ups' => 
        array (
          'access_license_number' => NULL,
          'gateway_url' => 'http://www.ups.com/using/services/rave/qcostcgi.cgi',
          'gateway_xml_url' => 'https://onlinetools.ups.com/ups.app/xml/Rate',
          'tracking_xml_url' => 'https://www.ups.com/ups.app/xml/Track',
          'username' => NULL,
          'password' => NULL,
          'is_account_live' => '0',
          'shipper_number' => '764V3V',
          'debug' => '0',
        ),
        'usps' => 
        array (
          'gateway_url' => 'http://production.shippingapis.com/ShippingAPI.dll',
          'gateway_secure_url' => 'https://secure.shippingapis.com/ShippingAPI.dll',
          'userid' => NULL,
          'password' => NULL,
        ),
      ),
      'trans_email' => 
      array (
        'ident_custom1' => 
        array (
          'email' => 'customerservice@adenandanais.com',
          'name' => 'aden + anais',
        ),
        'ident_custom2' => 
        array (
          'email' => 'customerservice@adenandanais.com',
          'name' => 'aden + anais',
        ),
        'ident_general' => 
        array (
          'email' => 'customerservice@adenandanais.com',
          'name' => 'aden + anais',
        ),
        'ident_sales' => 
        array (
          'email' => 'customerservice@adenandanais.com',
          'name' => 'aden + anais',
        ),
        'ident_support' => 
        array (
          'email' => 'customerservice@adenandanais.com',
          'name' => 'aden + anais',
        ),
      ),
      'newrelicreporting' => 
      array (
        'general' => 
        array (
          'api_url' => 'https://api.newrelic.com/deployments.xml',
          'insights_api_url' => 'https://insights-collector.newrelic.com/v1/accounts/%s/events',
        ),
      ),
      'paypal' => 
      array (
        'wpp' => 
        array (
          'api_password' => NULL,
          'api_signature' => NULL,
          'api_username' => NULL,
          'sandbox_flag' => '0',
        ),
        'fetch_reports' => 
        array (
          'ftp_login' => NULL,
          'ftp_password' => NULL,
          'ftp_sandbox' => '0',
          'ftp_ip' => NULL,
          'ftp_path' => NULL,
        ),
        'general' => 
        array (
          'merchant_country' => NULL,
          'business_account' => NULL,
        ),
      ),
      'fraud_protection' => 
      array (
        'signifyd' => 
        array (
          'api_url' => 'https://api.signifyd.com/v2/',
          'api_key' => NULL,
        ),
      ),
      'sitemap' => 
      array (
        'generate' => 
        array (
          'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
        ),
      ),
      'crontab' => 
      array (
        'default' => 
        array (
          'jobs' => 
          array (
            'analytics_subscribe' => 
            array (
              'schedule' => 
              array (
                'cron_expr' => '0 * * * *',
              ),
            ),
          ),
        ),
      ),
    ),
    'websites' => 
    array (
      'web_jp' => 
      array (
        'currency' => 
        array (
          'import' => 
          array (
            'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'catalog' => 
        array (
          'productalert_cron' => 
          array (
            'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'sitemap' => 
        array (
          'generate' => 
          array (
            'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'system' => 
        array (
          'magento_scheduled_import_export_log' => 
          array (
            'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'contact' => 
        array (
          'email' => 
          array (
            'recipient_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'trans_email' => 
        array (
          'ident_general' => 
          array (
            'name' => 'aden + anais',
            'email' => 'japan@adenandanais.com',
          ),
          'ident_sales' => 
          array (
            'name' => 'aden + anais',
            'email' => 'japan@adenandanais.com',
          ),
          'ident_support' => 
          array (
            'name' => 'aden + anais',
            'email' => 'japan@adenandanais.com',
          ),
          'ident_custom1' => 
          array (
            'name' => 'aden + anais',
            'email' => 'japan@adenandanais.com',
          ),
          'ident_custom2' => 
          array (
            'name' => 'aden + anais',
            'email' => 'japan@adenandanais.com',
          ),
        ),
        'carriers' => 
        array (
          'ups' => 
          array (
            'username' => '0:2:zEZJhKnWnfTthhSkSwqp6ShBENhnxN2X:NfBm7KvQUu4+h1+QaXRrU/rUnHe/7AYGf7j7pXIdngY=',
            'password' => '0:2:hRworKbFPVZX2H7L3o0078sBMzP9cHWk:WXIKOUFqI4ZsgpVvTNiN2kTkiNpvPieEpXeoDPO69Uo=',
            'access_license_number' => '0:2:pjzHUAczl1mdykPszGjxEBYh1Eg4c3er:RtzeQYsWWxcvHhyPAqxy8rrwBFkZWXeMqkdbDooXmXs=',
          ),
        ),
        'payment' => 
        array (
          'paypal_express' => 
          array (
            'merchant_id' => NULL,
          ),
          'braintree' => 
          array (
            'merchant_id' => 'rfjg836sqb6dhkhk',
            'private_key' => '0:2:ZVg4H0CXVoH1P1MeclWtVrVRiZsdRvCH:gt8mmONCPFfh51ILfu78FJkHVdvs/rAuuUDcfvWktoc=',
          ),
        ),
        'paypal' => 
        array (
          'wpp' => 
          array (
            'api_username' => '0:2:gIBOz3CkhabW2O050NoL5ateZLhG7Jde:1K38NOqm/46eIGspx0Jf09It4AHGt9b6yUOh43mn560SmeOM+sTk0ynGXcphq6MEMtVOPOzbIOY2myhzct1rjA==',
            'api_password' => '0:2:iFSHylSGlTteyj9oXo0TojXdV4KSDOWL:s9Ug7BEW2cWUJgJvntZpU2B2YBb4gCO/lKwL+ivEOws=',
            'api_signature' => '0:2:jFjHQ1d2Lw69G6nukddym1B7VbQpPC0Z:lgjk5Ps6mgm/W6xlo+QCWuHCC/QZavFv2F51X/e3EkYwAHBQOPTSSraRO72WLWGEbwliZgEpGqYu3l1yQLQ1yQ==',
          ),
        ),
      ),
      'web_fr' => 
      array (
        'currency' => 
        array (
          'import' => 
          array (
            'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'catalog' => 
        array (
          'productalert_cron' => 
          array (
            'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'sitemap' => 
        array (
          'generate' => 
          array (
            'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'system' => 
        array (
          'magento_scheduled_import_export_log' => 
          array (
            'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'contact' => 
        array (
          'email' => 
          array (
            'recipient_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'trans_email' => 
        array (
          'ident_general' => 
          array (
            'name' => 'aden + anais',
            'email' => 'europehelp@adenandanais.com',
          ),
          'ident_sales' => 
          array (
            'name' => 'aden + anais',
            'email' => 'europehelp@adenandanais.com',
          ),
          'ident_support' => 
          array (
            'name' => 'aden + anais',
            'email' => 'europehelp@adenandanais.com',
          ),
          'ident_custom1' => 
          array (
            'name' => 'aden + anais',
            'email' => 'europehelp@adenandanais.com',
          ),
          'ident_custom2' => 
          array (
            'name' => 'aden + anais',
            'email' => 'europehelp@adenandanais.com',
          ),
        ),
        'carriers' => 
        array (
          'ups' => 
          array (
            'username' => '0:2:uBRA6C1YK41fONObiaDhxNsAdnMesRMM:1ey3SoA9dSP1BqBABztLJjmCO0V4kXsRklScG+pUWLQ=',
            'password' => '0:2:itcfW33xYVCDzhEHh7PFL75OlHTDpvgy:KGevYpXMHsiUSJNJk6fPeqYUcvhn5sSOzFIeDYxAcQs=',
            'access_license_number' => '0:2:OjEAdyY2jqvJy0gFYVazT5e4DXyTiFhX:OG0Ba7rVv4Ji2EUfiV7JRizGSwmBLV1AFOSA7Rf3go0=',
          ),
        ),
        'payment' => 
        array (
          'paypal_express' => 
          array (
            'merchant_id' => NULL,
          ),
          'braintree' => 
          array (
            'merchant_id' => 'rfjg836sqb6dhkhk',
            'private_key' => '0:2:neemK9VL2DX7j65O4Mq1Qc436qf2XNMa:ff1sR5jdhUUzM5FviIlWQJ0+nAkVFLPTc5tmWAmvVZg=',
          ),
        ),
        'paypal' => 
        array (
          'wpp' => 
          array (
            'api_username' => '0:2:UM3eHLbQhxtOUJHjTmNnmHrExyfYahmV:gM1FySth6M6pwi1mZvTsx02HyQl73a6/bIZF3yAczzVpvQyiTA2pPOFy2xzzDq8V9sn0RVE/QVjAoQJszujykg==',
            'api_password' => '0:2:Ug0rR18ZprD91biKoVXBtf5RDbGEiSq2:rLit6DgF6NIw+lVAeItqohmSU0n7k2mgbbEfqOb+rc4=',
            'api_signature' => '0:2:YgkG8jwnAZnr1v2fhQGBVCioDOTLx9Dm:qO536jSgOfpufvflJybChc0MNIkfyJMqXnxcTPpN5lU6NUMvX8sTZXptgXeoKFctypLaI6h+3mhe0q+UVD1Y5g==',
            'sandbox_flag' => '1',
          ),
        ),
      ),
      'web_de' => 
      array (
        'currency' => 
        array (
          'import' => 
          array (
            'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'catalog' => 
        array (
          'productalert_cron' => 
          array (
            'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'sitemap' => 
        array (
          'generate' => 
          array (
            'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'system' => 
        array (
          'magento_scheduled_import_export_log' => 
          array (
            'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'contact' => 
        array (
          'email' => 
          array (
            'recipient_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'trans_email' => 
        array (
          'ident_general' => 
          array (
            'name' => 'aden + anais',
            'email' => 'europehelp@adenandanais.com',
          ),
          'ident_sales' => 
          array (
            'name' => 'aden + anais',
            'email' => 'europehelp@adenandanais.com',
          ),
          'ident_support' => 
          array (
            'name' => 'aden + anais',
            'email' => 'europehelp@adenandanais.com',
          ),
          'ident_custom1' => 
          array (
            'name' => 'aden + anais',
            'email' => 'europehelp@adenandanais.com',
          ),
          'ident_custom2' => 
          array (
            'name' => 'aden + anais',
            'email' => 'europehelp@adenandanais.com',
          ),
        ),
        'carriers' => 
        array (
          'ups' => 
          array (
            'username' => '0:2:zYy7JTLfbqsun6guVXE0UHvTehGFYozn:EbbTZA+0IeKdjS4RQg//tAy4PpRdnHS5tDr2TULlR3Q=',
            'password' => '0:2:dXlMHWRId92q58LRWfHGM2pR9WmXaLbd:bXku+VzgNMRyrniYQXb17Adgg9A07bI2iwkwvf23DAc=',
            'access_license_number' => '0:2:zmP79xFcwytsx49j9GPLz5szRFmRhoVH:HqXAtpWE213UGm9vPQ6RYWCERXb6QAaLgDqBo7VGUiE=',
          ),
        ),
        'payment' => 
        array (
          'paypal_express' => 
          array (
            'merchant_id' => NULL,
          ),
          'braintree' => 
          array (
            'merchant_id' => 'rfjg836sqb6dhkhk',
            'private_key' => '0:2:EkebA0vmc7nP7wkBfiRQXqI4ds55Qotk:rPP9ssNQiNXFdvIzE1XgyqwyNx3CrgloZczXApP0PPE=',
          ),
        ),
        'paypal' => 
        array (
          'wpp' => 
          array (
            'api_username' => '0:2:yymZoHbrFo7DKh4QpMwd3576o224glfF:j+TEOBwviLe9u9nUvDXDwpjo5b9D84xifHCUpO6jlilXUI15e0iuAPciUwyMU+EzkMynh3Jg5QGgeGtmcE157A==',
            'api_password' => '0:2:KruYZwguLeXmlS2BuoEnkCkyucsBox6Y:JGdAEjhcYH9aduzCEhAtZm0J5hRV6Tpcp8+cmSi2c5k=',
            'api_signature' => '0:2:PrNENUZoYMAautBPH63SydgSfzkuXhiC:vV4jOEQS9VIeoeuhkATmTaN0s/H4HbbMTvDlu8T02HaMqVfgvfFA62y2WkozLboAP68Wyng/tJBrLpjiSe6ECA==',
            'sandbox_flag' => '1',
          ),
        ),
      ),
      'web_uk' => 
      array (
        'currency' => 
        array (
          'import' => 
          array (
            'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'catalog' => 
        array (
          'productalert_cron' => 
          array (
            'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'sitemap' => 
        array (
          'generate' => 
          array (
            'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'system' => 
        array (
          'magento_scheduled_import_export_log' => 
          array (
            'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'contact' => 
        array (
          'email' => 
          array (
            'recipient_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'trans_email' => 
        array (
          'ident_general' => 
          array (
            'name' => 'aden + anais',
            'email' => 'europehelp@adenandanais.com',
          ),
          'ident_sales' => 
          array (
            'name' => 'aden + anais',
            'email' => 'europehelp@adenandanais.com',
          ),
          'ident_support' => 
          array (
            'name' => 'aden + anais',
            'email' => 'europehelp@adenandanais.com',
          ),
          'ident_custom1' => 
          array (
            'name' => 'aden + anais',
            'email' => 'europehelp@adenandanais.com',
          ),
          'ident_custom2' => 
          array (
            'name' => 'aden + anais',
            'email' => 'europehelp@adenandanais.com',
          ),
        ),
        'carriers' => 
        array (
          'ups' => 
          array (
            'username' => '0:2:P79N3TQUPaRThLY531Ni6MiWttfl8psN:Do3M37JvW+Cp41ijaqkbP8PuokNME0CgPDEHTRWpYQg=',
            'password' => '0:2:mrqfa70Q7IAfjoadgNmcpuZJN5VLkdpx:gteRRgmdIigMaCkwe4vLSAT4KQfP51QtF5Q2T7aG6vk=',
            'access_license_number' => '0:2:uFDvCubA2CFcQGfWkrZAMP9qKU2VYhji:3lBmdnyj3/+UYHOKABB3GmBwSnVu4zAVRqBMfxfR48s=',
          ),
        ),
        'payment' => 
        array (
          'paypal_express' => 
          array (
            'merchant_id' => NULL,
          ),
          'braintree' => 
          array (
            'merchant_id' => 'rfjg836sqb6dhkhk',
            'private_key' => '0:2:brrEoHanF3vIOyx8tcWVm2SSPIerFwHG:p4nuhSbuODCMisM/LdkJ2brki8gGbR5hjAo8B3BRB84=',
          ),
        ),
        'paypal' => 
        array (
          'wpp' => 
          array (
            'api_username' => '0:2:OYb2wbg24CBJ0YHk1u6dnPW2o0kTnRp1:1TRhutZWBIW6e3kQKu16qEF4w7IBAFm1r/LgVEzhGaK0LiHes4xTiFCL1uVqMmSRoMpYaAVAExL7EmA66RA/vQ==',
            'api_password' => '0:2:FqU2s1UntlWk9uv0Or417QTlH34VKjMg:cTCJprNZQj6Y7yP6cxPj/u14XBCRiZtA9djw4mtsDOE=',
            'api_signature' => '0:2:Ax8ToT7I4UT4eeVTwQKtxuE5oyQZIs69:Y+783oPMD2GJKCteNMDnbrwtmDZrL7ps33uJ/mSE4dkaVXUGTLYloQEs98NP58kGkODeplTzjSPe681mkF1GoA==',
          ),
        ),
      ),
      'web_ca' => 
      array (
        'currency' => 
        array (
          'import' => 
          array (
            'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'catalog' => 
        array (
          'productalert_cron' => 
          array (
            'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'sitemap' => 
        array (
          'generate' => 
          array (
            'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'system' => 
        array (
          'magento_scheduled_import_export_log' => 
          array (
            'error_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'contact' => 
        array (
          'email' => 
          array (
            'recipient_email' => 'project-adenanais@somethingdigital.onmicrosoft.com',
          ),
        ),
        'trans_email' => 
        array (
          'ident_general' => 
          array (
            'name' => 'aden + anais',
            'email' => 'customerservice@adenandanais.com',
          ),
          'ident_sales' => 
          array (
            'name' => 'aden + anais',
            'email' => 'customerservice@adenandanais.com',
          ),
          'ident_support' => 
          array (
            'name' => 'aden + anais',
            'email' => 'customerservice@adenandanais.com',
          ),
          'ident_custom1' => 
          array (
            'name' => 'aden + anais',
            'email' => 'customerservice@adenandanais.com',
          ),
          'ident_custom2' => 
          array (
            'name' => 'aden + anais',
            'email' => 'customerservice@adenandanais.com',
          ),
        ),
        'carriers' => 
        array (
          'ups' => 
          array (
            'username' => '0:2:OMoSacKkt6TRW0mKLmaviepL1Ou9jCXY:EKl0dkOet9M0Zqf6UX3hgWWCBhGztgSDs4fKkdEhk40=',
            'password' => '0:2:ebGfdhpwd9eZZqABDAWLEcmvRHv09iPe:8NBB6n9R8wzdqoTbG6qhE15eAnnlZugUVcQQi9CeuE8=',
            'access_license_number' => '0:2:jljnszJwzNlo4LQxbD8FGl0nSmeSuTXD:ObT0VD2HhXLu4J1PTwKArJhtW9Q4dMv3DdKetGoTQN4=',
          ),
        ),
        'payment' => 
        array (
          'paypal_express' => 
          array (
            'merchant_id' => NULL,
          ),
          'braintree' => 
          array (
            'merchant_id' => 'rfjg836sqb6dhkhk',
            'private_key' => '0:2:vwZVGTtUqqbMqX41GEEwDzcmcb5BZOOl:CyG/Zh6CSPELLsPlAe3OQA+ndeWRCRjIqbtMxuaAPw8=',
          ),
        ),
        'paypal' => 
        array (
          'general' => 
          array (
            'business_account' => NULL,
          ),
          'wpp' => 
          array (
            'api_username' => '0:2:aE7HnqrEHsgXgbOMFj98IblLdgc2UReV:2LF7yxo/oOrVkBAMQL2R+RjpxewxHcBM55Rs24wUOQfiWHo6ZblHoxn6ODUVZc/pbkw4YoKY3vao3je32MHT+Q==',
            'api_password' => '0:2:lbszsJ3Z2aN8brLGBKF9LQLPXNIIvMtH:prPz2CHFjELK1JpURpDn4eQzlXL/0Oez/x3lTOh7MkE=',
            'api_signature' => '0:2:OM66l0We0zc1RNyio4i0KTFyxd6SPqqu:693IUC4J/5OoKKnOzUU5DgWIc1b3AI0QBoReS+oZKECSehALpUVr0+dKHP3EfPh4R1va5IEhYtQdMNyRe5NCDw==',
            'sandbox_flag' => '1',
          ),
        ),
      ),
      'base' => 
      array (
        'carriers' => 
        array (
          'ups' => 
          array (
            'username' => '0:2:6txv5Dmz3nN0lFGtjPTdZjD4qTciV9QR:WRKT8rrLe/BOfY1o0j/Z0JmJpW6IDkIpFsWHUUvwtpk=',
            'password' => '0:2:tddpHpPADsqPXX87CSas1Eninqq8p7QJ:PhwyM1KGwYKs+EgzXOWrh5yfLchbOfHWno+VRWceAFI=',
            'access_license_number' => '0:2:aUYIaE9DWqjJehHH0HZRbd0pthoJe4if:pu60ICffsPuuVKRgUwBpts3+DO1cyUBcAcLaG9ga+Ts=',
          ),
        ),
        'payment' => 
        array (
          'paypal_express' => 
          array (
            'merchant_id' => NULL,
          ),
          'braintree' => 
          array (
            'merchant_id' => 'rfjg836sqb6dhkhk',
            'private_key' => '0:2:d0JpAe5yEVl46FUis4n9kRryD6BNW6FZ:ODf8WkldraVbnQ0pZxA7U6WXhKycvRvUM/DM5ZGsAko=',
          ),
        ),
        'paypal' => 
        array (
          'general' => 
          array (
            'business_account' => NULL,
          ),
          'wpp' => 
          array (
            'api_username' => '0:2:8ZPdqVJudhnzcvbZ5Za1hZF3kcF0UpsT:iMcpc+rfRqjxJolrUjcyHgWmiMexhWLET0muPg82PNeyuS53rmK8Hr1IawClQWksMILmrKZcaX7lU8oRr8IUFw==',
            'api_password' => '0:2:f8WvUwPXD3nGppvkevcmlHfvKLmv1Fe6:Ioj5hvrt7IZvNzoFSu8c4OPdePQpGHwWPqWFOzVW0s8=',
            'api_signature' => '0:2:D0son8cR2qnhFJsK5uWg22CDEOYvj2sN:LQPHdzQSKEJGg5Nzan9TtJvtbI8eqkxtGzqdpGu5WkCgaTzH01JXGq1gitQOLQ7KD53FhQT91XODwsGMqQyctQ==',
            'sandbox_flag' => '1',
          ),
        ),
      ),
    ),
  ),
  'cron_consumers_runner' => 
  array (
    'cron_run' => false,
    'max_messages' => 10000,
    'consumers' => 
    array (
    ),
  ),
);
