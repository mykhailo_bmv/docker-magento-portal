#!/bin/bash

docker-compose down

docker_magento=" -f magento.docker-compose.yml"
docker_portal=" -f portal.docker-compose.yml"

# For elasticsearch
permission="sudo chmod -R 0777 "
magento_elastic="docker/data/magento-elastic/"
portal_elastic="docker/data/portal-elastic/"


sudo sysctl -w vm.max_map_count=262144

if [ "$#" == 0 ]; then
   eval "docker-compose $docker_magento up --build -d"
   eval "docker-compose $docker_magento ps"
   eval "$permission $magento_elastic"
   exit
fi

if [ "$1" == 'ao' ] && [ "$2" == 'portal' ]; then
   eval "docker-compose $docker_magento $docker_portal up"
   eval "docker-compose $docker_magento $docker_portal ps"
   eval "$permission $magento_elastic $portal_elastic"
   exit
fi

if [ "$1" == 'portal' ]; then
   eval "docker-compose $docker_portal up -d"
   eval "docker-compose $docker_portal ps"
   eval "$permission $portal_elastic"
   exit
fi

echo "Wrong argument! Used args is [ao portal]"